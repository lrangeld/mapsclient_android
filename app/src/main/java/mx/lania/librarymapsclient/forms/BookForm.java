package mx.lania.librarymapsclient.forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.lania.librarymapsclient.MainActivity;
import mx.lania.librarymapsclient.R;
import mx.lania.librarymapsclient.info.ProviderInfo;
import mx.lania.librarymapsclient.info.TablesInfo;

public class BookForm extends AppCompatActivity {

    private EditText titleTxt;
    private EditText availTxt;
    private EditText priceTxt;
    private EditText authorTxt;
    private Button btnAddBookData;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_form);

        //Enlazar componentes view
        titleTxt = (EditText) findViewById(R.id.titleTxt);
        availTxt = (EditText) findViewById(R.id.availTxt);
        priceTxt = (EditText) findViewById(R.id.priceTxt);
        authorTxt = (EditText) findViewById(R.id.authorTxt);
        btnAddBookData = (Button) findViewById(R.id.btnAddBookData);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        //Evento del boton
        btnAddBookData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insertar libro
                ContentValues newBookData = new ContentValues();
                newBookData.put(TablesInfo.TITLE_FIELD, titleTxt.getText().toString().trim());
                newBookData.put(TablesInfo.AVAILABILITY_FIELD, availTxt.getText().toString().trim());
                newBookData.put(TablesInfo.PRICE_FIELD, priceTxt.getText().toString().trim());
                newBookData.put(TablesInfo.AUTHOR_FIELD, authorTxt.getText().toString().trim());

                Uri uri = getContentResolver().insert(ProviderInfo.SELECT_CONTENT_BOOK_URI, newBookData);
                if(uri != null) {
                    showMsg("Registro agrgado en:\n"+ uri);
                }

                titleTxt.setText("");
                availTxt.setText("");
                priceTxt.setText("");
                authorTxt.setText("");
                showMsg("Libro guardado correctamente");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BookForm.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void displayBooks(Cursor c) {
        Toast.makeText(getBaseContext(),
                "ID: " + c.getString(0) + "\n"
                        + "Titulo: " + c.getString(1) + "\n"
                        + "Disponibilidad: " + c.getString(2) + "\n"
                        + "Precio: " + c.getString(3) + "\n"
                        + "Autor: " + c.getString(4) + "\n",
                Toast.LENGTH_LONG).show();
    }

    public void showMsg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}