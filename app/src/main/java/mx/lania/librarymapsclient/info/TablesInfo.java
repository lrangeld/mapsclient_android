package mx.lania.librarymapsclient.info;

public class TablesInfo {

    //Campos de tabla
    public static final String ID_FIELD = "_id";
    public static final String TITLE_FIELD = "titulo";
    public static final String AVAILABILITY_FIELD = "disponibilidad";
    public static final String PRICE_FIELD = "precio";
    public static final String AUTHOR_FIELD = "autor";
    //Informacion de la tabla
    public static final String TABLE_BOOKS = "libros";
    //Consulta de eliminacion
    public static final String BOOK_DROP_TABLE = "DROP TABLE IF EXISTS "+ TABLE_BOOKS;

    //Campos de tabla
    //public static final String ID_FIELD = "_id";
    public static final String NAME_FIELD = "nombre";
    public static final String BIRTHDATE_FIELD = "fecha_nacimiento";
    public static final String ADDRESS_FIELD = "direccion";
    public static final String REGISTRY_FIELD = "fecha_registro";
    public static final String LATITUDE_FIELD = "latitud";
    public static final String LONGITUDE_FIELD = "longitud";
    //Informacion de la tabla
    public static final String TABLE_MEMBERS = "miembros";
    //Consulta de eliminacion
    public static final String MEMBER_DROP_TABLE = "DROP TABLE IF EXISTS "+ TABLE_MEMBERS;
}
