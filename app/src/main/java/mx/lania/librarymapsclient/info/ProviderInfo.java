package mx.lania.librarymapsclient.info;

import android.net.Uri;

public class ProviderInfo {

    //Datos del provedor de libros
    public static final String PROVIDER_BOOK_NAME = "mx.lania.librarymaps.customproviders.book-provider";
    public static final String URL_BOOK = "content://" + PROVIDER_BOOK_NAME + "/books";
    public static final Uri SELECT_CONTENT_BOOK_URI = Uri.parse(URL_BOOK);
    //Datos del provedor de miembros
    public static final String PROVIDER_MEMBER_NAME = "mx.lania.librarymaps.customproviders.member-provider";
    public static final String URL_MEMBER = "content://" + PROVIDER_MEMBER_NAME + "/members";
    public static final Uri SELECT_CONTENT_MEMBER_URI = Uri.parse(URL_MEMBER);
    public  static final String SELECT_BY_NAME_MEMBER_URL = "content://" + PROVIDER_MEMBER_NAME + "/members-name/";
    public  static final String SELECT_BY_ID_MEMBER_URL = "content://" + PROVIDER_MEMBER_NAME + "/members/";
}
